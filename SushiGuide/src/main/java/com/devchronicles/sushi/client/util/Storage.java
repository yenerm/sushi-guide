package com.devchronicles.sushi.client.util;

import java.sql.Date;

import com.google.gwt.user.client.Cookies;

public class Storage {
	
	public static final String TOKEN="TOKEN";
	
	public static class LocalStorage {

		public native static String key(int index) /*-{
			return $wnd.localStorage.key(index);
		}-*/;
		public native static void setItem(String key, String value) /*-{
			$wnd.localStorage.setItem(key, value);
		}-*/;

		public native static String getItem(String key) /*-{
			return $wnd.localStorage.getItem(key);
		}-*/;

		public native static void removeItem(String key) /*-{
			$wnd.localStorage.removeItem(key);
		}-*/;

		public native static void clear() /*-{
			$wnd.localStorage.clear();
		}-*/;
	}
	public static class SecureCookie {
		
		public static String getItem(String key){
			return Cookies.getCookie(key);
		}
		
		@SuppressWarnings("deprecation")
		public static void setItem(String key, String value){
			Cookies.setCookie(key, value, new Date(2050, 1, 1), "", "", true);
		}
	}
}
